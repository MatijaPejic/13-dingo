
# Project 13-Dingo

Korisnik bira restoran na osnovu nekih karakteristika i rezerviše svoje mesto u izabranom restoranu

## Developers

- [Darko Nešković, 208/2016](https://gitlab.com/darko123455)
- [Matija Pejić, 185/2016](https://gitlab.com/MatijaPejic)
- [Kristina Pantelić, 91/2016](https://gitlab.com/beskonacnost)
- [Jelena Milivojević, 4/2016](https://gitlab.com/ratspeaker)
- [Dara Milojković, 100/2016](https://gitlab.com/DaraMilojkovic)
